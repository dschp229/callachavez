package Messages;

import java.util.*;

public class Operation {

	public static boolean containedValues(Integer array1[], Integer array2[], Integer arrayMerged[]){
		assert((array1 != null) && (array2 != null) && (array1 != null));

		List<Integer> arrayTemp = Arrays.asList(array1);
		arrayTemp.addAll(Arrays.asList(array2));
		Collections.sort(arrayTemp);		

		int index = 0;	

		for(; index < arrayMerged.length ; index++){			
			if(arrayMerged[index] != arrayTemp.get(index))
				break;				
		}

		return (index == arrayMerged.length);
	}

	public static boolean isOrdered(Integer arr[]){
		assert(arr != null)

		for(int index=0; index < (arr.length-1); index++)
			if( (arr[index].compareTo(arr[index+1])) > 0) return false;			
		
		return true;
	}

	public static Integer [] merge(Integer []left, Integer []right){
		assert((left != null) && (right != null));

		int sizeTotal = left.length + right.length;	

		Integer [] arryMerged = new Integer [sizeTotal];
		int indexM = 0;
		int indexL = 0;
		int indexR = 0;

		while(indexL < left.length && indexR < right.length){
			if(left[indexL] <= right[indexR]){
				
				assert(left[indexL] <= right[indexR]);				
				arryMerged[indexM++] = left[indexL++];
				
			}else{
				
				assert(left[indexL] > right[indexR]);
				arryMerged[indexM++] = right[indexR++];
			}
		}

		for(int i = indexL; i < left.length; i++)
			arryMerged[indexM++] = left[indexL++];

		for(int j = indexR; j < right.length; j++)
			arryMerged[indexM++] = right[indexR++];	
		
		assert(arryMerged != null);
		return arryMerged;
	}


}
