package Messages;
class Receiver extends Thread{

	private Port<ArrayMessage<Integer>> p;


	public Receiver(Port<T> p){ 
		this.p = p;
	}

	public void run(){

		System.out.println("Esperando recibir array 1");		
		ArrayMessage<Integer> message = p.receive();
		System.out.println("Array Recibido 1");
		assert(message != null);
		assert(Operation.isOrdered(message.getArr()) == true);


		System.out.println("Esperando recibir array 2");
		ArrayMessage<Integer> message2 = p.receive();
		System.out.println("Array Recibido 2");
		assert(message2 != null);
		assert(Operation.isOrdered(message2.getArr()) == true);		

		Integer arrMer[] = Operation.merge(message.getArr(), message2.getArr());		

		message.setArr(arrMer);
		message2.setArr(arrMer);

		p.reply(message);		
		p.reply(message2);
	}

}
