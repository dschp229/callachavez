package Messages;
import java.util.*;

public class Port<T extends Message> {
  int nextId= 0;
  int served= 0;
  List<T> msgList= new ArrayList<>();
  int maxsize;

  public Port() { this(100); }

  public Port(int maxsize) {
    this.maxsize= maxsize;
  }

  public synchronized T receive() {
    try {
      while (msgList.isEmpty())
        wait();
      notifyAll();
      T e=msgList.get(0);
      msgList.remove(0);
      return e;
    }
    catch (InterruptedException excp) {
      return null;
    }
  }

  public synchronized T receive(long delay) {
    try {
      long ini= System.currentTimeMillis();
      long elapsed= 0;
      while (msgList.isEmpty() && elapsed<delay) {
        wait(delay-elapsed);
        elapsed= System.currentTimeMillis()-ini;
      }
      if (msgList.isEmpty())
        return null;
      else {
        notifyAll();
        T e=msgList.get(0);
        msgList.remove(0);
        return e;
      }
    }
    catch (InterruptedException excp) {
      return null;
    }
  }

  public void send(T msg) {
    submit(msg);
    waitReply(msg);
  }

  public synchronized void submit(T msg) {
    try {
      int id= nextId++;
      while (id>served || msgList.size()>=maxsize)
        wait();
      served++;
      msgList.add(msg);
      msg.isReplied= false;
      notifyAll();
    }
    catch (InterruptedException excp) {
    }
  }

  public void waitReply(T msg) {
    try {
      synchronized (msg) {
        while (!msg.isReplied)
          msg.wait();
      }
    }
    catch (InterruptedException excp) {
    }
  }

  public void reply(T msg) {
    synchronized (msg) {
      msg.isReplied= true;
      msg.notifyAll();
    }
  }
}
