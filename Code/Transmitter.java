package Messages;
class Transmitter extends Thread{

	private Port<ArrayMessage<Integer>> p;


	public Transmitter(Port<ArrayMessage<Integer>> p){
		this.p = p; 
	}


	public void run(){

		Integer[] e= {0,1,2,3,4,5};
		ArrayMessage<Integer> message = new ArrayMessage<>(e);
		
		System.out.println("SIZE BEFORE: "+ p.getArr().length);
		System.out.println("Enviando el Array");
		p.send(message);
		assert(message != null);
		assert(Operation.isOrdered(message.getArr()) == true);
				
		System.out.println("Array enviado, sigo con mis lineas! ");
		System.out.println("SIZE AFTER: "+ p.getArr().length);
	}
}
