package Messages;

public class ArrayMessage<T extends Comparable<T>> extends Message{

	private T[] arr;
	private int lenght;

	public ArrayMessage(T[] a) {
		this.arr=a;
		this.lenght=a.length;
	}

	public T[] getArr() {
		return this.arr;
	}

	public int getLenght() {
		return lenght;
	}

	@Override
	public String toString() {
		return this.lenght+"";	
	}

	public void setArr(T []arr){
		this.arr = arr;
		this.lenght = arr.length;
	}

}
