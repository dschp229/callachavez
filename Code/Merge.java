package Messages;

public class Merge<T extends Comparable<T>> {
	ArrayMessage<T> arrayFinal;
	public ArrayMessage<T> merge(ArrayMessage<T> arr1,ArrayMessage<T> arr2) {
		assert arr1.getLenght()>0:"El primer arreglo debe contener al menos un elemento";//Precondiciones
		assert arr2.getLenght()>0:"El segundo arreglo debe contener al menos un elemento";//Precondiciones
		assert arr1.isOrdered();//Invariante
		assert arr2.isOrdered();//Invariante
		assert arrayFinal.getLenght()==arr1.getLenght()+arr2.getLenght():"El array final debe ser de la longitud del array 1 y 2";//Postcondicion
		return arrayFinal;
	}
	
}
