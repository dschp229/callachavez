package Messages;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MergeMessagesTest {

	Port<ArrayMessage<Integer>> p= new Port<ArrayMessage<Integer>>();
	Port<ArrayMessage<Integer>> p2= new Port<ArrayMessage<Integer>>();
	Port<ArrayMessage<Integer>> p3= new Port<ArrayMessage<Integer>>();
	Receiver receiver = new Receiver(p);		
	Transmitter  transmitter1 = new Transmitter(p);
	Transmitter  transmitter2 = new Transmitter(p);


	//Probando si una gran cantidad de Transmitters envian mensajes
	@Test
	public void multipleTransmittersTest() throws InterruptedException{
		MultithreadedStressTester stressTester = new MultithreadedStressTester(5);
		stressTester.stress(new Runnable() {
			public void run() {
				Integer[] e= {0,1,2,3,4,5};
				ArrayMessage<Integer> m=new ArrayMessage<>(e);
				p.send(m);;
			}
		});
		stressTester.shutdown();
		assertEquals(stressTester.totalActionCount(), p.msgList.size());
	}


	/* verificando el tama�o final del arreglo ordenado por merge sort */
	@Test
	public void testFinalSizeMerged(){	

		Integer arr1[] = {1,4,7,23,54,77, 86};
		Integer arr2[] = {5,8,11,23,30,74};

		Integer arrMer[] = Operation.merge(arr1, arr2);			

		assertEquals("Tama�o Final del Arreglo: ", arrMer.length ,(arr1.length + arr2.length));
	}	

	/* verificando el orden final del arreglo ordenado por merge sort */
	@Test
	public void testOrderFinalMerged(){

		Integer arr1[] = {1,4,7,23,54,77, 86};
		Integer arr2[] = {5,8,11,23,30,74};

		Integer arrMer[] = Operation.merge(arr1, arr2);			

		assertEquals("Orden Final del Arreglo: ", Operation.isOrdered() , true);
	}

	/* verificando el contenido inicial con el final del arreglo ordenado por merge sort */
	@Test
	public void testContainedValuesMerged(){		

		Integer arr1[] = {1,4,7,23,54,77, 86};
		Integer arr2[] = {5,8,11,23,30,74};

		Integer arrMer[] = Operation.merge(arr1, arr2);	

		assertEquals("Contiene valores Iniciales: ", Operation.containedValues(arr1, arr2,arrMer ) , true);
	}
	
	
	
	@Test
	void breackfreeTransmitters() {
		//Transmitter 1
		(new Thread(new Runnable(){
			public void run(){
				Integer[] e= {0,1,2,3,4,5};
				ArrayMessage<Integer> m=new ArrayMessage<>(e);
				p.send(m);
				System.out.println("Transmitter1: Mensaje enviado, sigo con mi vida!");
			}}))
		.start();
		//Transmitter 2
		(new Thread(new Runnable(){
			public void run(){
				Integer[] e= {6,7,8,9,10,11};
				ArrayMessage<Integer> m=new ArrayMessage<>(e);
				p2.send(m);
				System.out.println("Transmitter2: Mensaje enviado, sigo con mi vida!");
			}}))
		.start();
		//Receiver 
		(new Thread(new Runnable(){
			public void run(){
				Integer[] e= {0,1,2,3,4,5};
				ArrayMessage<Integer> m=new ArrayMessage<>(e);
				System.out.println("Receiver: Esperando recibir un mensajes");
				ArrayMessage<Integer> mt1 = p.receive();
				ArrayMessage<Integer> mt2= p.receive();
				System.out.println("Receiver: Mensajes recibido");
				System.out.println("Receiver: Enviando el mensaje");
				p3.send(m);//Transmitir mensaje 
				System.out.println("Receiver: sigo con mi vida");
				p.reply(mt1); //Libera transmitter 1
				p2.reply(mt2);//Libera transmitter 2
			}}))
		.start();
		(new Thread(new Runnable(){
			public void run(){
				System.out.println("Esperando recibir un mensaje del receiver");
				ArrayMessage<Integer> message = p3.receive();
				System.out.println("Mensaje recibido");
				p3.reply(message); //Libera receiver
			}}))
		.start();
	}


}
